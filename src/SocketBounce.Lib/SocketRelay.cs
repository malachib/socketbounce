﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace SocketBounce.Lib
{
    public class SocketRelay
    {
        readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        public event Action Started;

        public event Action Stopped;

        public async void Start(int clientPort, int serverPort)
        {
            var address = new IPAddress(new byte[] { 192, 168, 2, 137 });
            //var address = IPAddress.Loopback;
            await Start(address, clientPort, serverPort);
        }

        /// <summary>
        /// Client refers to the "producer", e.g. Android emulator
        /// Server refers to the "consumer", e.g. Debugger to plug into emulator
        /// </summary>
        /// <param name="clientAddress"></param>
        /// <param name="clientPort"></param>
        /// <param name="serverPort"></param>
        /// <returns></returns>
        public async Task Start(IPAddress clientAddress, int clientPort, int serverPort)
        {
            var clientEndpoint = new IPEndPoint(clientAddress, clientPort);
            var tcpClient = new TcpClient();
            tcpClient.Connect(clientEndpoint);
            var clientStream = tcpClient.GetStream();

            var serverEndpoint = new IPEndPoint(System.Net.IPAddress.Any, serverPort);
            var tcpListener = new TcpListener(serverEndpoint); 

            tcpListener.Start();
            TcpClient tcpServer = await tcpListener.AcceptTcpClientAsync();
            var serverStream = tcpServer.GetStream();
            var ct = cancellationTokenSource.Token;

            // trouble with built-in CopyToAsync is it requires one additional byte
            // to copy before the cancel can take affect.  This could be troublesome
            // for aborting a stalled connection
            //var pushToClient = serverStream.CopyToAsync(clientStream, 100, ct);
            //var pushToServer = clientStream.CopyToAsync(serverStream, 100, ct);

            var pushToClient = serverStream.CopyToAsync2(clientStream, ct); //, () => tcpServer.Client.Available);
            var pushToServer = clientStream.CopyToAsync(serverStream, ct);

            if (Started != null)
                Started();

            await pushToClient;
            await pushToServer;

            tcpListener.Stop();
            tcpClient.Client.Disconnect(false);
            tcpServer.Client.Disconnect(false);

            if (Stopped != null)
                Stopped();
        }

#if DEBUG
        volatile int debugVal = 0;
#endif
        public void Stop()
        {
            cancellationTokenSource.Cancel();
        }
    }


    public static class StreamRelay
    {
        /// <summary>
        /// vNext-friendly and only waits up to 3s to enact cancellationtoken
        /// This is only pseudo-async, it actually performs everything on a worker thread
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="destination"></param>
        /// <param name="cancellationToken"></param>
        public static async Task CopyToAsync(this System.IO.Stream stream, System.IO.Stream destination, CancellationToken cancellationToken, Func<int> acquireRemainingCount = null)
        {
            // for fast connections, async writes slow things down due to
            // thread overhead.  AsyncWrite is for slower connections
            bool asyncWrite = true;
            var task = Task.Factory.StartNew(
                //async 
                () =>
            {
                stream.ReadTimeout = 3000;

                byte[] buffer;
                byte[] bufferA = new byte[10000];
                byte[] bufferB = asyncWrite ? new byte[10000] : null;
                bool usingBufferA = true;

                buffer = bufferA;

                Task writeTask = null;

                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        int pos = 0;

                        try
                        {
                            //pos = stream.Read(buffer, 0, buffer.Length);
                            pos = stream.Read(buffer, 0, buffer.Length);
                        }
                        // Can't do IOException because it appears in both System.IO and System.Runtime
                        // for vNext
                        catch (Exception e)
                        {
                            // FIX: documentation mentions some exceptions here mean .Read was NOT advanced.
                            // be sure we're handling that properly

                            // Since the above case is true, we (for now) presume that we are getting
                            // an IOException by virtue of having a SocketException InnerException
                            var se = e.InnerException as SocketException;

                            // If this is NOT a timeout, then it's an actual error
                            if (se.SocketErrorCode != SocketError.TimedOut)
                            {
                                // TODO: log error
                                // throw; // consider rethrowing, except that can be kinda funky when 
                                // performed on worker threads
                                return; // we're outta here
                            }

                            // arriving here =
                            // we assess this to be a soft/recoverable error (timeout) 
                            // so we eat it and try again.  This is our vNext-friendly
                            // way of doing gentle polling every 3s, being that Thread.Sleep
                            // is not presently available to us
                        }

                        if (asyncWrite)
                        {
                            if (writeTask != null)
                                writeTask.Wait(); // "await" causes a problem, maybe control is flushed out and thread ends?

                            writeTask = destination.WriteAsync(buffer, 0, pos, cancellationToken);

                            // double-buffering to not overwrite buffer during async dest.write
                            // slightly memory intensive, but faster (no GC needed)
                            if (usingBufferA)
                                buffer = bufferB;
                            else
                                buffer = bufferA;

                            usingBufferA = !usingBufferA;
                        }
                        else
                        {
                            destination.Write(buffer, 0, pos);
                        }
                    }
                    // Can't do IOException because it appears in both System.IO and System.Runtime
                    // for vNext
                    catch (Exception e)
                    {
                        // vNext kludge, for now
                        //var eType = e.GetType();
                        //if(eType.Name != "IOException")
                        {
                            var se = e.InnerException as SocketException;

                            if (se.SocketErrorCode != SocketError.TimedOut)
                            {
                                // TODO: log error
                                return; // we're outta here
                            }
                        }
                        // we assess this to be a soft/recoverable error (timeout) 
                        // so we eat it and try again.  This is our vNext-friendly
                        // way of doing gentle polling every 3s
                    }
                }
                // we only get here when cancellationtoken is fired off
                // allegedly passing in cancellationToken on the StartNew() begets
                // Task.StartNew to eat exception and rethrow it, updating Task status
                // along the way
                //
                // although inconsistent with how regular CopyToAsync operates, I am
                // going to NOT throw this here for now until I understand the mechanism
                // better (all it does now is seem to make my debugger mad and catch
                // the exception here)
                //cancellationToken.ThrowIfCancellationRequested();
            }, cancellationToken);
            await task;
        }

        /// <summary>
        /// vNext-friendly and only waits up to 3s to enact cancellationtoken
        /// Fully "async"
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="destination"></param>
        /// <param name="cancellationToken"></param>
        public static async Task CopyToAsync2(this System.IO.Stream stream, System.IO.Stream destination, CancellationToken cancellationToken)
        {
            stream.ReadTimeout = 3000;

            byte[] buffer;
            byte[] bufferA = new byte[10000];
            byte[] bufferB = new byte[10000];
            bool usingBufferA = true;

            buffer = bufferA;

            Task writeTask = null;

            while (!cancellationToken.IsCancellationRequested)
            {
                int pos = 0;

                try
                {
                    //pos = stream.Read(buffer, 0, buffer.Length);
                    pos = await stream.ReadAsync(buffer, 0, buffer.Length);
                }
                // Can't do IOException because it appears in both System.IO and System.Runtime
                // for vNext
                catch (Exception e)
                {
                    // FIX: documentation mentions some exceptions here mean .Read was NOT advanced.
                    // be sure we're handling that properly

                    // Since the above case is true, we (for now) presume that we are getting
                    // an IOException by virtue of having a SocketException InnerException
                    var se = e.InnerException as SocketException;

                    // If this is NOT a timeout, then it's an actual error
                    if (se.SocketErrorCode != SocketError.TimedOut)
                    {
                        // TODO: log error
                        // throw; // consider rethrowing, except that can be kinda funky when 
                        // performed on worker threads
                        return; // we're outta here
                    }

                    // arriving here =
                    // we assess this to be a soft/recoverable error (timeout) 
                    // so we eat it and try again.  This is our vNext-friendly
                    // way of doing gentle polling every 3s, being that Thread.Sleep
                    // is not presently available to us
                }

                if (writeTask != null)
                    await writeTask; // "await" causes a problem, maybe control is flushed out and thread ends?

                writeTask = destination.WriteAsync(buffer, 0, pos, cancellationToken);

                // double-buffering to not overwrite buffer during async dest.write
                // slightly memory intensive, but faster (no GC needed)
                if (usingBufferA)
                    buffer = bufferB;
                else
                    buffer = bufferA;

                usingBufferA = !usingBufferA;
            }
            // we only get here when cancellationtoken is fired off
            // allegedly passing in cancellationToken on the StartNew() begets
            // Task.StartNew to eat exception and rethrow it, updating Task status
            // along the way
            //
            // although inconsistent with how regular CopyToAsync operates, I am
            // going to NOT throw this here for now until I understand the mechanism
            // better (all it does now is seem to make my debugger mad and catch
            // the exception here)
            //cancellationToken.ThrowIfCancellationRequested();
        }

        static int debugVal = 0;
    }
}