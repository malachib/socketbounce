﻿using SocketBounce.Lib;
using System;

namespace SocketBounce.ConsoleApp
{
    public class Program
    {
        public void Main(string[] args)
        {
            var relay = new SocketRelay();

            relay.Start(23, 24);

            Console.WriteLine("Listener started (allegedly)");
            Console.ReadLine();
            relay.Stop();
            Console.WriteLine("Listener stopped (allegedly)");
            Console.ReadLine();
        }
    }
}
