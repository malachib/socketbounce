﻿using System;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Routing;
using Microsoft.Framework.DependencyInjection;

// Picking up some pointers from
// http://weblogs.asp.net/jongalloway/a-30-minute-look-at-asp-net-vnext
namespace SocketBounce.WebHost
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app)
        {
            //app.UseErrorPage();

            app.UseServices(services =>
            {
                services.AddMvc();
            });

            app.UseMvc(routes =>
            {
                //routes.MapRoute("Home", "Home/index.cshtml");
                //routes.Routes.Add(new DefaultRouter)

                /*
                routes.MapRoute(
                    name: "home",
                    template: "{controller}/{action}",
                    defaults: new { controller = "Home", action = "Index" }
                );*/
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //app.UseWelcomePage();
            app.UseDefaultFiles();
            //app.UseStaticFiles();
        }
    }
}
