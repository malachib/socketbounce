﻿using Microsoft.AspNet.Mvc;
using SocketBounce.WebHost.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SocketBounce.WebHost.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        [HttpGet]
        public IActionResult Index()
        {
            return View(_User());
            //return View("~/Home/index.cshtml");
        }

        public User _User()
        {
            User user = new User()
            {
                Name = "My name",
                Address = "My address"
            };

            return user;
        }

        //[HttpPost(Name = "start")]

        [HttpPost]
        [ActionName("Start")]
        [AcceptVerbs("post")]
        public IActionResult Start()
        {
            return View(_User());
        }

        [HttpPost]
        [ActionName("Stop")]
        [AcceptVerbs("post")]
        public IActionResult Stop()
        {
            return View(_User());
        }

        [HttpPost]
        [ActionName("Start")]
        [AcceptVerbs("post")]
        public IActionResult Start(string clientPort, string listenPort)
        {
            return View(_User());
        }

    }
}
