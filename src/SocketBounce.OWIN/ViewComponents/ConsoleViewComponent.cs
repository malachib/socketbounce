﻿using Microsoft.AspNet.Mvc;
using System;

namespace SocketBounce.WebHost.ViewComponents
{
    public class ConsoleViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var data = new { ClientPort = 23, ListenPort = 24 };
            return View(data);
        }
    }
}